<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;
use Egulias\EmailValidator\EmailValidator;
use Egulias\EmailValidator\Validation\DNSCheckValidation;
use Egulias\EmailValidator\Validation\MultipleValidationWithAnd;
use Egulias\EmailValidator\Validation\RFCValidation;

class UpdateCostumerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'rif' => 'string|required|max:15',
            'rif' => function($attribute,$value,$fail){
                if(!isset($this->rif))
                    $fail ('Rif es un campo obligatorio');
                elseif (strlen($this->rif) > 15 || strlen($this->rif) < 6) 
                    $fail ('Este campo debe estar entre 6 y 15 caracteres');

                $validation = DB::table('costumers')->select('id', 'rif')->where('rif',$this->rif)->first();

                if (($validation) && ($this->id <> $validation->id))
                    $fail ('Este rif ya esta siendo usado');
            },
            'name' => 'string|required|max:50',
             // 'email' => 'string|required|email:rfc,dns',
            'email' => function($attribute,$value,$fail){
                $validation = DB::table('costumers')->select('id', 'email')->where('email',$this->email)->first();
                if (($validation) && ($this->id <> $validation->id))
                    $fail ('Este email ya esta siendo usado');
                    
                $validator = new EmailValidator();
                $multipleValidations = new MultipleValidationWithAnd([
                new RFCValidation(),
                new DNSCheckValidation()
                ]);
                if(!$validator->isValid($this->email, $multipleValidations)){
                    $fail ('Correo electronico no valido');
                }
            },
            'phonenumber' => 'string|required|max:15',
            'status' =>'required',
            'password' =>'string|required'
        ];
    }


    public function messages()
    {
        return [
            'rif.required' => 'Rif es un campo obligatorio',
            'name.required' => 'Nombre de la empresa es un campo obligatorio',
            'email.required' => 'Correo es un campo obligatorio',
            'phonenumber.required' => 'Telefono es un campo obligatorio',
            'status.required' => 'Estatus es un campo obligatorio',
            'password.required' => 'Contraseña de empresa es un campo obligatorio',
            'max' => 'Maximo numero de caracteres alcanzado, :max',
            'email.email' => 'Correo electronico no valido',
            'string' => 'Debe ser una cadena de caracteres',

        ];
    }
}

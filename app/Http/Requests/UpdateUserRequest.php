<?php

namespace App\Http\Requests;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;
use Egulias\EmailValidator\EmailValidator;
use Egulias\EmailValidator\Validation\DNSCheckValidation;
use Egulias\EmailValidator\Validation\MultipleValidationWithAnd;
use Egulias\EmailValidator\Validation\RFCValidation;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'dni' => 'string|required|between:6,15',
            'dni' => function($attribute,$value,$fail){
                if(!isset($this->dni))
                    $fail ('Cedula es un campo obligatorio');
                elseif (strlen($this->dni) > 15 || strlen($this->dni) < 6) 
                    $fail ('Este campo debe estar entre 6 y 15 caracteres');

                $validation = DB::table('users')->select('id', 'dni')->where('dni',$this->dni)->first();

                if (($validation) && ($this->id <> $validation->id))
                    $fail ('Esta cedula ya esta siendo usada');
            },
            'name' => 'string|required|between:1,30',
            'lastname' => 'string|required|between:1,30',
            // 'username' => 'string|required|between:4,20',
            'username' => function($attribute,$value,$fail){
                if(!isset($this->username))
                    $fail ('Nombre de usuario es un campo obligatorio');
                elseif (strlen($this->username) > 20 || strlen($this->username) < 4) 
                    $fail ('Este campo debe estar entre 4 y 20 caracteres');

                $validation = DB::table('users')->select('id', 'username')->where('username',$this->username)->first();

                if (($validation) && ($this->id <> $validation->id))
                    $fail ('Esta nombre de usuario ya esta siendo usado');
            },
            // 'email' => 'string|required|email:rfc,dns',
            'email' => function($attribute,$value,$fail){
            $validation = DB::table('users')->select('id', 'email')->where('email',$this->email)->first();
            // dd($validation);
            // dd($this->id);
            if (($validation) && ($this->id <> $validation->id))
                $fail ('Este email ya esta siendo usado');
                
            $validator = new EmailValidator();
            $multipleValidations = new MultipleValidationWithAnd([
            new RFCValidation(),
            new DNSCheckValidation()
            ]);
            if(!$validator->isValid($this->email, $multipleValidations)){
                $fail ('Correo electronico no valido');
            }
            },
            'phonenumber' => 'string|required|between:10,15',
            'password'=>'string|required|between:8,20',
            'is_admin' =>'required',
            'status' =>'required'
        ];
    }


    public function messages()
    {
        return [
            'dni.required' => 'Cedula es un campo obligatorio',
            'name.required' => 'Nombre es un campo obligatorio',
            'lastname.required' => 'Apellido es un campo obligatorio',
            'username.required' => 'Nombre de usuario es un campo obligatorio',
            'email.required' => 'Correo es un campo obligatorio',
            'phonenumber.required' => 'Telefono es un campo obligatorio',
            'password.required' => 'Contraseña es un campo obligatorio',
            'is_admin.required' => 'Rol es un campo obligatorio',
            'status.required' => 'Estatus es un campo obligatorio',
            'between' => 'Este campo debe estar entre :min y :max caracteres',
            'email.email' => 'Correo electronico no valido',
            'string' => 'Debe ser una cadena de caracteres',
        ];
    }
}

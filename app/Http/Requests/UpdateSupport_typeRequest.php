<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSupport_typeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description'=>'string|required|between:1,50'
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Este campo es requerido',
            'string' => 'Debe ser una cadena de caracteres',
            'between' => 'Este campo debe estar entre :min y :max caracteres',
        ];
    }
}

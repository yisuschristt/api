<?php

namespace App\Http\Requests;
use App\Models\User;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        'dni' => 'string|required|unique:users|between:6,15',
        'name' => 'string|required|between:1,30',
        'lastname' => 'string|required|between:1,30',
        'username' => 'string|required|unique:users|between:4,20',
        'email' => 'string|required|unique:users|email:rfc,dns',
        'phonenumber' => 'string|required|between:10,15',
        'password'=>'string|required|between:8,20',
        'is_admin' =>'required',
        'status' =>'required'
        ];
    }

        /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function messages()
    {
        return [
            'dni.required' => 'Cédula es un campo obligatorio',
            'name.required' => 'Nombre es un campo obligatorio',
            'lastname.required' => 'Apellido es un campo obligatorio',
            'username.required' => 'Nombre de usuario es un campo obligatorio',
            'email.required' => 'Correo es un campo obligatorio',
            'phonenumber.required' => 'Teléfono es un campo obligatorio',
            'password.required' => 'Contraseña es un campo obligatorio',
            'is_admin.required' => 'Rol es un campo obligatorio',
            'status.required' => 'Estatus es un campo obligatorio',
            'dni.unique' => 'Ésta cédula está siendo utilizado por otro cliente',
            'username.unique' => 'Éste nombre de usuario está siendo utilizado por otro cliente',
            'email.unique' => 'Éste correo electrónico está siendo utilizado por otro cliente',
            'between' => 'Éste campo debe estar entre :min y :max caracteres',
            'email.email' => 'Correo electrónico no valido',
            'string' => 'Debe ser una cadena de caracteres',
        ];
    }
}

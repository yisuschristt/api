<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCaseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'required|date',
            'requested_by' => 'string|required|max:250',
            'subject' => 'string|required|max:250',
            'description' => 'string|required',
            'magnitude' => 'required|IN:1,2,3',
            'attachment'=> 'nullable',
            'status' => 'string|required|IN:Pendiente,Procesado,Anulado',
            'user_id' => 'nullable|exists:App\Models\User,id',
            'costumer_id'=>'required|exists:App\Models\Costumer,id',
            'support_type_id' =>'nullable|exists:App\Models\Support_type,id'
        ];
    }


    public function messages()
    {
        return [
            'date.required' => 'Fecha del caso es un campo obligatorio',
            'requested_by.required' => 'Solicitado por es un campo obligatorio',
            'subject.required' => 'Titulo del caso es un campo obligatorio',
            'description.required' => 'Descripcion del caso es un campo obligatorio',
            'magnitude.required' => 'Urgencia del caso es un campo obligatorio',
            'status.required' => 'Estatus del caso es un campo obligatorio',
            'costumer_id.required' => 'Correo es un campo obligatorio',
            'support_type_id.required' => 'Telefono es un campo obligatorio',
            'max' => 'Maximo numero de caracteres alcanzado, :max caracteres maximos',
            'string' => 'Debe ser una cadena de caracteres',
            'date' => 'Debe ingresar una fecha valida',

        ];
    }
}

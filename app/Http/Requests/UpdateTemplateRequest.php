<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTemplateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'review' => 'string|required|max:250',
            'procedure' => 'string|required|max:250',
            ];
    }


    public function messages()
    {
        return [
            'review.required' => 'Analisis del caso es un campo obligatorio',
            'procedure.required' => 'Procedimiento del caso es un campo obligatorio',
            'max' => 'Maximo numero de caracteres alcanzado, :max',
            'string' => 'Debe ser una cadena de caracteres',
        ];
    }

}

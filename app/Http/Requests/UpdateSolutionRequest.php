<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSolutionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        'date_solution' => 'required|date',
        'review' => 'string|required|max:250',
        'procedure' => 'string|required|max:250',
        'time'=>'numeric|required',
        'case_id' =>'required|exists:App\Models\Cases,id'
        ];
    }

    public function messages()
    {
        return [
            'date_solution.required' => 'Solucion del caso es un campo obligatorio',
            'review.required' => 'Analisis del caso es un campo obligatorio',
            'procedure.required' => 'Procedimiento del caso es un campo obligatorio',
            'time.required' => 'Horas de solución es un campo obligatorio',
            'case_id.required' => 'Caso relacionado es un campo obligatorio',
            'max' => 'Maximo numero de caracteres alcanzado, :max',
            'string' => 'Debe ser una cadena de caracteres',
            'numeric' => 'Debe ingresar caracteres numericos',
            
            

        ];
    }
}

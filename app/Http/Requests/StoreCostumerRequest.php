<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCostumerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'rif' => 'string|required|unique:costumers|max:15',
            'name' => 'string|required|max:50',
            'email' => 'string|required|unique:costumers|email:rfc,dns',
            'phonenumber' => 'string|required|max:15',
            'status' =>'required',
            'password' =>'string|required|unique:costumers'
        ];
    }
        public function messages()
    {
        return [
            'rif.required' => 'Rif es un campo obligatorio',
            'name.required' => 'Nombre de la empresa es un campo obligatorio',
            'email.required' => 'Correo es un campo obligatorio',
            'phonenumber.required' => 'Telefono es un campo obligatorio',
            'password.required' => 'Contraseña de empresa es un campo obligatorio',
            'status.required' => 'Estatus es un campo obligatorio',
            'rif.unique' => 'Este Rif esta siendo utilizado por otro cliente',
            'email.unique' => 'Este correo electronico esta siendo utilizado por otro cliente',
            'password.unique' => 'Esta contraseña de empresa esta siendo utilizado por otro cliente',
            'max' => 'Maximo numero de caracteres alcanzado, :max',
            'email.email' => 'Correo electronico no valido',
            'string' => 'Debe ser una cadena de caracteres',
            
            

        ];
    }
}

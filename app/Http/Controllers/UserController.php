<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use JWTAuth;

class UserController extends Controller
{


    // public function __construct()
    // {
    //     $this->middleware('admin');
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users= User::whereIn('status', [$request->status])->orderBy('id','desc')->get();
        return $users;
        // $users= User::whereIn('status', [$request->status])->orderBy('id','desc')->get();
        // return $users;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreUserRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        $users = User::create($request->all());
        return response()->json("Guardado Exitosamente",200);
     }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $users = User::findOrFail($id);
        return $users;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users = User::findOrFail($id);
        return $users;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateUserRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $id)
    {
        $users= User::find($id);
        $users->name = $request->name;
        $users->lastname = $request->lastname;
        $users->dni = $request->dni;
        $users->email = $request->email;
        $users->phonenumber = $request->phonenumber;
        $users->username = $request->username;
        $users->is_admin = $request->is_admin;
        $users->status = $request->status;
        $users->save();
        return response()->json("Modificado Exitosamente",200);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $users = User::findOrFail($id);
        DB::table('users')->where('id', $users->id)->update(array('status' => 'I'));
        return response()->json("Usuario inhabilitado Exitosamente",200);
    }
}

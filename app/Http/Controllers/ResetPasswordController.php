<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use App\Mail\ResetPasswordCode;
use Illuminate\Support\Facades\Mail;

class ResetPasswordController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function emailvalidation(Request $request){

        $validation = User::select('email')->where('email', [$request->email])->first();
        if (!is_null($validation)) {

            $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

            $code = substr(str_shuffle($permitted_chars), 0, 10);

            DB::table('users')->where('email', $request->email)->update(array('resetcode' => $code));

            $records = User::select('email','name','lastname','resetcode','username')->where('email', [$request->email])->first();

            Mail::to($request->email)->send(new ResetPasswordCode($records));
            
            return response()->json("Codigo de recuperación enviado al correo electronico: ".$request->email,200);
        }
        else{

            return response()->json("Correo electronico inexistente",412);
        }


    }


    public function codevalidation(Request $request){

        $validation = User::select('resetcode')->where('resetcode', [$request->resetcode])->first();
        if (!is_null($validation)) {

           return True;
        }
        else{

            return response()->json("Codigo de verificacion invalido",412);
        }

    }


    public function changepassword(Request $request){
    DB::table('users')->where('resetcode', $request->resetcode)->update(array('password' => $request->newpassword,'resetcode' => ""));
    }
}

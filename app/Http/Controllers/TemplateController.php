<?php

namespace App\Http\Controllers;
use App\Models\Template;
use Illuminate\Support\Facades\Validator;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class TemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $templates= Template::orderBy('id','desc')->get();
        return $templates;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $test=new Template; /// create model object
        $validator = Validator::make($request->all(),$test->rules);
        if($validator->fails()){
            return response()->json($validator->errors(),412);
        }
        $templates = Template::create($request->all());
        return response()->json("Guardado Exitosamente",200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $templates = Template::findOrFail($id);
        return $templates;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $templates = Template::findOrFail($id);
        return $templates;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $test=new Template; /// create model object
        $validator = Validator::make($request->all(),$test->rules);
        if($validator->fails()){
            return response()->json($validator->errors(),412);
        }
        $templates= Template::find($id);
        $templates->update($request->all());
        return $templates;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $templates = Template::findOrFail($id);
        $templates->delete();
        return response()->json("Eliminado Exitosamente",200);
    }
}

<?php

namespace App\Http\Controllers;
use App\Models\Support_type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\StoreSupport_typeRequest;
use App\Http\Requests\UpdateSupport_typeRequest;
use JWTAuth;


class Support_typeController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $support_types= Support_type::orderBy('id','desc')->get();
        return $support_types;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\StoreSupport_typeRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSupport_typeRequest $request)
    {
        // $test=new Support_type; /// create model object
        // $validator = Validator::make($request->all(),$test->rules);
        // if($validator->fails()){
        //     return response()->json($validator->errors(),412);
        // }
        $support_types = Support_type::create($request->all());
        return response()->json("Guardado Exitosamente",200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $support_types = Support_type::findOrFail($id);
        return $support_types;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $support_types = Support_type::findOrFail($id);
        return $support_types;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\UpdateSupport_typeRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSupport_typeRequest $request, $id)
    {
        // $test=new Support_type; /// create model object
        // $validator = Validator::make($request->all(),$test->rules);
        // if($validator->fails()){
        //     return response()->json($validator->errors(),412);
        // }
        $support_types= Support_type::find($id);
        $support_types->update($request->all());
        return $support_types;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $support_types = Support_type::findOrFail($id);
        $support_types->delete();
        return response()->json("Eliminado Exitosamente",200);
    }
}

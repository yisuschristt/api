<?php

namespace App\Http\Controllers;
use App\Models\Costumer;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Notifications\Notifiable;
use App\Http\Requests\StoreCostumerRequest;
use App\Http\Requests\UpdateCostumerRequest;

class CostumerController extends Controller
{


    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $costumers= Costumer::whereIn('status', [$request->status])->orderBy('id','desc')->get();
        return $costumers;
        // $costumers= Costumer::whereIn('status', [$request->status])->orderBy('id','desc')->get();
        // return $costumers;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\StoreCostumerRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCostumerRequest $request)
    {
        $costumers = Costumer::create($request->all());
        return response()->json("Guardado Exitosamente",200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $costumers = Costumer::findOrFail($id);
        return $costumers;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $costumers = Costumer::findOrFail($id);
        return $costumers;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\UpdateCostumerRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCostumerRequest $request, $id)
    {
        $costumers= Costumer::find($id);
        $costumers->rif = $request->rif;
        $costumers->name = $request->name;
        $costumers->email = $request->email;
        $costumers->phonenumber = $request->phonenumber;
        $costumers->status = $request->status;
        $costumers->save();
        return response()->json("Modificado Exitosamente",200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $costumers = Costumer::findOrFail($id);
        DB::table('costumers')->where('id', $costumers->id)->update(array('status' => 'I'));
        return response()->json("Empresa inhabilitada Exitosamente",200);
    }
}

<?php

namespace App\Http\Controllers;
use App\Models\Cases;
use App\Models\Costumer;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Notifications\Notifiable;
use Illuminate\Http\Request;
use App\Http\Requests\StoreCaseRequest;
use App\Http\Requests\UpdateCaseRequest;
use Illuminate\Support\Facades\Storage;

class OutsideController extends Controller
{

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getcostumer(Request $request)
    {
        $costumers= Costumer::where('password','=',$request->password)->where('status','=','A')->get();
        $costumers = json_decode($costumers);
        if (empty($costumers)) {

            return response()->json("No existe ninguna empresa registrada activa con esa contraseña",412);
        }
        return $costumers;
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\StoreCaseRequest  $request
     * @return \Illuminate\Http\Response
     */

    public function sendcase(StoreCaseRequest $request)
    {
        $cases = Cases::create($request->all());
        if ($request->file('attachment')){
            $path = Storage::disk('public')->put('attached', $request->file('attachment'));
            $cases->fill(['attachment' => asset($path)])->save();
        }

        // $cases->tags()->sync($request->get('attachment'))

        return response()->json("Guardado Exitosamente",200);
    }


       /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function numbercasescostumer(Request $request){

        $count = DB::table('cases')->select('status', DB::raw('count(*) as total'))->where('costumer_id','=',$request->costumer_id)
        ->groupBy('status')
        ->get();
        $count = json_decode($count);
        return $count;
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function casescostumer(Request $request)
    {

        $cases = Cases::with('costumers','users','support_types','solutions')
        ->whereIn('status', [$request->status])
        ->where('costumer_id','=',$request->costumer_id)
        ->orderBy('id','desc')
        ->get();
        return $cases;
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function voidcase($id)
    {
            $cases = Cases::findOrFail($id);
            // *dd($cases->user_id);
            if(is_null($cases->user_id)){
                DB::table('cases')->where('id', $cases->id)->update(array('status' => 'Anulado'));
                return response()->json("Anulado Exitosamente",200);
            }
            else{
                DB::table('cases')->where('id', $cases->id)->update(array('status' => 'En Discusión'));
                return response()->json("Solicitud de anulación enviada",200);
            }

            
    }




    
    
}

<?php

namespace App\Http\Controllers;
use App\Models\Solution;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Notifications\Notifiable;
use Illuminate\Http\Request;
use App\Http\Requests\StoreSolutionRequest;
use App\Http\Requests\UpdateSolutionRequest;

class SolutionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $solutions = Solution::orderBy('id','desc')->get();
        return $solutions;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\StoreSolutionRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSolutionRequest $request)
    {
        // $test=new Solution; /// create model object
        // $validator = Validator::make($request->all(),$test->rules);
        // if($validator->fails()){
        //     return response()->json($validator->errors(),412);
        // }
        $solutions = Solution::create($request->all());
        DB::table('cases')->where('id', $request->case_id)
        ->update(array('status' => 'Procesado'));
        return response()->json("Caso resuelto exitosamente, encuentrelo ahora en el area de casos procesados",200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $solutions = Solution::findOrFail($id);
        return $solutions;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $solutions = Solution::findOrFail($id);
        return $solutions;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\UpdateSolutionRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSolutionRequest $request, $id)
    {

        // $test=new Solution; /// create model object
        // $validator = Validator::make($request->all(),$test->rules);
        // if($validator->fails()){
        //     return response()->json($validator->errors(),412);
        // }
        $solutions = Solution::findOrFail($id);
        $solutions->date_solution = $request->date_solution;
        $solutions->review = $request->review;
        $solutions->procedure = $request->procedure;
        $solutions->time = $request->time;
        $solutions->case_id = $request->case_id;
        $solutions->save();
        // $solutions->update($request->all());
        return $solutions;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $solutions = Solution::findOrFail($id);
        $solutions->delete();
    }
}

<?php

namespace App\Http\Controllers;
use App\Models\Cases;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Notifications\Notifiable;
use Illuminate\Http\Request;
use App\Http\Requests\StoreCaseRequest;
use App\Http\Requests\UpdateCaseRequest;
use Illuminate\Support\Facades\Storage;
use JWTAuth;
class CaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $admin = JWTAuth::parseToken()->getClaim(['username','is_admin','id']);
        // $cases= Cases::with('costumers','users','support_types','solutions')->whereIn('status', [$request->status])->orderBy('id','desc')->get();
        // return $cases;
        if ($admin[1]==1)
        {
            // $cases= Cases::with('costumers','users','support_types','solutions')->where('status','=','Pendiente')->orderBy('id','desc')->get();
            $cases = Cases::with('costumers','users','support_types','solutions')->whereIn('status', [$request->status])->orderBy('id','desc')->get();
        }
        else
        {
            $cases= Cases::with('costumers','users','support_types','solutions')->where('user_id','=',$admin[2])->whereIn('status', [$request->status])->orderBy('id','desc')->get();
        }
        return $cases;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\StoreCaseRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCaseRequest $request)
    {
     
        $cases = Cases::create($request->all());
        if ($request->file('attachment')){
            $path = Storage::disk('public')->put('attached', $request->file('attachment'));
            $cases->fill(['attachment' => asset($path)])->save();
        }

        // $cases->tags()->sync($request->get('attachment'))

        return response()->json("Guardado Exitosamente",200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cases = Cases::with('costumers','users','support_types','solutions')->find($id);
        return $cases;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cases = Cases::with('costumers','users','support_types','solutions')->findOrFail($id);
        return $cases;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\UpdateCaseRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCaseRequest $request, $id)
    {
        $cases = Cases::find($id);
        $cases->date = $request->date;
        $cases->requested_by = $request->requested_by;
        $cases->subject = $request->subject;
        $cases->description = $request->description;
        $cases->magnitude = $request->magnitude;
        $cases->attachment = $request->attachment;
        $cases->status = $request->status;
        $cases->user_id = $request->user_id;
        $cases->costumer_id = $request->costumer_id;
        $cases->support_type_id = $request->support_type_id;
        $cases->save();
        if ($request->file('attachment')){
            $path = Storage::disk('public')->put('attached', $request->file('attachment'));
            $cases->fill(['attachment' => asset($path)])->save();
        }
        // $cases->update($request->all());
        return $cases;
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            $cases = Cases::findOrFail($id);
            DB::table('cases')->where('id', $cases->id)->update(array('status' => 'Anulado'));
            return response()->json("Anulado Exitosamente",200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function refuseanulation($id)
    {
            $cases = Cases::findOrFail($id);
            DB::table('cases')->where('id', $cases->id)->update(array('status' => 'Pendiente'));
            return response()->json("Solicitud de anulación declinada",200);
    }


     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function numbercases(){

        $user = JWTAuth::parseToken()->getClaim(['username','is_admin','id']);
        $count = DB::table('cases')->select('status', DB::raw('count(*) as total'))->where('user_id','=',$user[2])
        ->groupBy('status')
        ->get();
      
        return $count;
    }
}

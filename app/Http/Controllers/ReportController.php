<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use App\Models\Cases;
use App\Models\User;
use App\Models\Costumer;
use App\Models\Support_type;
use JWTAuth;

class ReportController extends Controller
{
    public function summarycostumer(Request $request)
    {
        // dd($request);
        // $from = date($request->rango[0]);
        // $to   = date($request->rango[1]);
        $from = date($request->input('from', null));
        $to   = date($request->input('to', null));

        $cases = Cases::with(['users', 'solutions', 'costumers', 'support_types']);


        if (!is_null($request->user_id)) {
            $cases->where('user_id', $request->user_id);
        }
        
        if (!is_null($request->customer_id)) {
            $cases->where('costumer_id', $request->customer_id);
        }
        // dd($request->status);
        if (isset($request->status)) {
            $cases->whereIn('status', [$request->status]);
        }

        if(!is_null($from) && !is_null($to)) {
            $cases->whereBetween('date', [$from, $to]);
        }
       

        $summary = $cases->get()->toArray();
        for ($i=0;$i<count($summary);$i++){
            if (is_null($summary[$i]['users'])){
                $summary[$i]['users'] = array(
                    'name' => 'No tiene',
                    'lastname' => 'usuario asignado'
                );
            }
        }
        $summary = array_map(function($e) {
            if (is_null($e['solutions'])){
                return [
                    'id' => $e['id'],
                    'date' => $e['date'],
                    'requested_by' => $e['requested_by'],
                    'subject' => $e['subject'],
                    'description' => $e['description'],
                    'magnitude' => $e['magnitude'],
                    'support_type' => $e['support_types']['description'],
                    'customer' => $e['costumers']['name'],
                    'name' => $e['users']['name'].' '.$e['users']['lastname'],
                    // array('costumers' => [
                    //     'rif' => $e['costumers']['rif']
                    // ])
                ];
            }else{
                return [
                    'id' => $e['id'],
                    'date' => $e['date'],
                    'requested_by' => $e['requested_by'],
                    'subject' => $e['subject'],
                    'description' => $e['description'],
                    'magnitude' => $e['magnitude'],
                    'support_type' => $e['support_types']['description'],
                    'date_solution' => $e['solutions']['date_solution'],
                    'review' => $e['solutions']['review'],
                    'procedure' => $e['solutions']['procedure'],
                    'time' => $e['solutions']['time'],
                    'customer' => $e['costumers']['name'],
                    'name' => $e['users']['name'].' '.$e['users']['lastname'],
                    // array('costumers' => [
                    //     'rif' => $e['costumers']['rif']
                    // ])
                ];
            }
        }, $summary);
        // dd($summary);
        return $summary;
    }


    public function summarysession(Request $request)
    {
        // dd($request);
        // $from = date($request->rango[0]);
        // $to   = date($request->rango[1]);
        $user = JWTAuth::parseToken()->getClaim('id');
        //  dd($user);
        $from = date($request->input('from', null));
        $to   = date($request->input('to', null));

        $cases = Cases::with(['users', 'solutions', 'costumers', 'support_types']);

        if (!is_null($user)) {
            $cases->where('user_id', $user);
        }
        
        if (!is_null($request->customer_id)) {
            $cases->where('costumer_id', $request->customer_id);
        }

        if (isset($request->status)) {
            $cases->where('status', $request->status);
        }

        if(!is_null($from) && !is_null($to)) {
            $cases->whereBetween('date', [$from, $to]);
        }


        $summary = $cases->get()->toArray();

        //  dd($summary);

        $summary = array_map(function($e) {
            if (is_null($e['solutions'])){
            return [
                'id' => $e['id'],
                'date' => $e['date'],
                'requested_by' => $e['requested_by'],
                'subject' => $e['subject'],
                'description' => $e['description'],
                'magnitude' => $e['magnitude'],
                'support_type' => $e['support_types']['description'],
                'customer' => $e['costumers']['name'],
                'name' => $e['users']['name'].' '.$e['users']['lastname'],

            ];
        }else{
            return [
                'id' => $e['id'],
                'date' => $e['date'],
                'requested_by' => $e['requested_by'],
                'subject' => $e['subject'],
                'description' => $e['description'],
                'magnitude' => $e['magnitude'],
                'support_type' => $e['support_types']['description'],
                'date_solution' => $e['solutions']['date_solution'],
                'review' => $e['solutions']['review'],
                'procedure' => $e['solutions']['procedure'],
                'time' => $e['solutions']['time'],
                'customer' => $e['costumers']['name'],
                'name' => $e['users']['name'].' '.$e['users']['lastname'],
                // array('costumers' => [
                //     'rif' => $e['costumers']['rif']
                // ])
            ];
        }
        }, $summary);

        return $summary;
       
    }

    public function allusers(Request $request){
        $users= User::where('status','=',[$request->status])->orderBy('id','desc')->get();
        return $users;
    }

    public function allcostumers(Request $request){
        $costumers= Costumer::where('status','=',[$request->status])->orderBy('id','desc')->get();
        return $costumers;
    }

    public function allsupport_types(){
        $support_types= Support_type::orderBy('id','desc')->get();
        return $support_types;
    }



    

    





}

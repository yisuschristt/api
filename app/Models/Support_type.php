<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Support_type extends Model
{
    use HasFactory;
    protected $table = 'support_types';


    public function cases()
    {
        return $this->hasMany(Cases::class, 'support_type_id', 'id');
    }

        /**Primary Key/ */

        protected $primaryKey = 'id';

         /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

        protected $fillable = [
            'description'
           
        ];

        // public $rules = [
            
        //     'description'=>'string|required'
        // ];
    


}

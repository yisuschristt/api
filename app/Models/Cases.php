<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cases extends Model
{
    use HasFactory;
    protected $table = 'cases';

    public function solutions()
    {
        return $this->hasOne(Solution::class, 'case_id', 'id');
    }
    public function users()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function costumers()
    {
        return $this->belongsTo(Costumer::class, 'costumer_id', 'id');
    }
    public function support_types()
    {
        return $this->belongsTo(Support_type::class, 'support_type_id', 'id');
    }

        /**Primary Key/ */

        protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
        protected $fillable = [
            'date',
            'requested_by',
            'subject',
            'description',
            'attachment',
            'magnitude',
            'status',
            'user_id',
            'costumer_id',
            'support_type_id'
        ];
    
        // public $rules = [
        //     'date' => 'required',
        //     'requested_by' => 'string|required',
        //     'subject' => 'string|required',
        //     'description' => 'string|required',
        //     'magnitude' => 'required',
        //     'status' => 'string|required',
        //     'user_id' => 'required',
        //     'costumer_id'=>'required',
        //     'support_type_id' =>'required'
        // ];

}

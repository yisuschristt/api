<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Solution extends Model
{
    use HasFactory;
    protected $table = 'solutions';

    /**Primary Key/ */

    protected $primaryKey = 'id';

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date_solution',
        'review',
        'procedure',
        'time',
        'case_id'
    ];

    // public $rules = [
    //     'date_solution' => 'required',
    //     'review' => 'string|required',
    //     'procedure' => 'string|required',
    //     'time'=>'required',
    //     'case_id' =>'required'
    // ];

}

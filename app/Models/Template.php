<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    use HasFactory;
    protected $table = 'templates';

      /**Primary Key/ */

      protected $primaryKey = 'id';

       /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'review',
        'procedure'
    ];

    public $rules = [
      'review' => 'string|required',
      'procedure'=>'string|required',
  ];

}

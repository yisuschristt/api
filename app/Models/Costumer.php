<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Costumer extends Model
{
    use HasFactory;
    protected $table = 'costumers';

    public function cases()
    {
        return $this->hasMany(Cases::class, 'costumer_id', 'id');
    }

        /**Primary Key/ */

        protected $primaryKey = 'id';


        protected $fillable = [
            'rif',
            'name',
            'email',
            'phonenumber',
            'status',
            'password'
        ];


        // public $rules = [
        //     'rif' => 'string|required|sometimes|unique:costumers',
        //     'name' => 'string|required',
        //     'email' => 'string|required|sometimes|unique:costumers',
        //     'phonenumber' => 'string|required',
        //     'status' =>'required'
        // ];
    


}

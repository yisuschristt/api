<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\User;
use Illuminate\Support\Facades\View;

class ResetPasswordCode extends Mailable
{
    use Queueable, SerializesModels;
    public $resetPassword;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $resetPassword)
    {
        // dd($resetPassword);
        $this->resetPassword = $resetPassword;

        // dd($this->resetPassword);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.reset_password');
    }
}

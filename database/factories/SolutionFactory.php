<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Cases;
use App\Models\Solution;
use Faker\Generator as Faker;

class SolutionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Solution::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $cases = Cases::pluck('id')->toArray();
        $case_id = $this->faker->unique()->randomElement($cases);
        $toModify = Cases::find($case_id);
        $toModify->status = 'Procesado';
        $toModify->save();
        return [
            'date_solution' => $this->faker->date($format = 'Y-m-d', $max = 'now'),
            'review' => $this->faker->text($maxNbChars = 100),
            'procedure' => $this->faker->text($maxNbChars = 200),
            'time' => $this->faker->numberBetween($min = 1, $max = 250),
            'case_id' => $case_id,
        ];
    }
}

<?php

namespace Database\Factories;


use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Cases;
use App\Models\User;
use App\Models\Costumer;
use App\Models\Support_type;
use Faker\Generator as Faker;

class CasesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Cases::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $users = User::pluck('id')->toArray();
        $costumers = Costumer::pluck('id')->toArray();
        $support_types = Support_type::pluck('id')->toArray();
        return [

            'date' => $this->faker->date($format = 'Y-m-d', $max = 'now'),
            'requested_by' => $this->faker->name,
            'subject' => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
            'description' => $this->faker->text($maxNbChars = 150),
            'attachment' => $this->faker->URL,
            'magnitude' => $this->faker->randomElement($array = array ('1','2','3')),
            'status' => $this->faker->randomElement($array = array ('Pendiente','Procesado','Anulado')),
            'user_id' => $this->faker->randomElement($users),
            'costumer_id' => $this->faker->randomElement($costumers),
            'support_type_id' => $this->faker->randomElement($support_types),
            
        ];
    }
}

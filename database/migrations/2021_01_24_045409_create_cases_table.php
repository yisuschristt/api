<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cases', function (Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->string('requested_by');
            $table->string('subject');
            $table->longText('description');
            $table->longText('attachment')->nullable();
            $table->enum('magnitude', ['1', '2','3']);
            $table->enum('status', ['Pendiente', 'Procesado','Anulado','En Discusión']);
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedBigInteger('costumer_id');
            $table->foreign('costumer_id')->references('id')->on('costumers');
            $table->unsignedBigInteger('support_type_id')->nullable();
            $table->foreign('support_type_id')->references('id')->on('support_types');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cases');
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Cases;
use App\Models\User;
use App\Models\Costumer;
use App\Models\Support_type;
use App\Models\Solution;
use App\Models\Template;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(1)->create();
        // \App\Models\Costumer::factory(15)->create();
        \App\Models\Support_type::factory(1)->create();
        // \App\Models\Cases::factory(20)->create();
        // \App\Models\Solution::factory(20)->create();
        // \App\Models\Template::factory(5)->create();
    }
}

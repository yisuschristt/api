<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('login', 'AuthController@login');
Route::post('getcostumer', 'OutsideController@getcostumer');
Route::post('verifyemail', 'ResetPasswordController@emailvalidation');
Route::post('verifycode', 'ResetPasswordController@codevalidation');
Route::post('passwordreset', 'ResetPasswordController@changepassword');
Route::post('sendcase', 'OutsideController@sendcase');
Route::delete('voidcases/{id}', 'OutsideController@voidcase');
Route::post('casescostumer', 'OutsideController@casescostumer');
Route::post('numbercasescostumer', 'OutsideController@numbercasescostumer');
Route::resource('templates', 'TemplateController');
Route::group(['middleware' => 'api','prefix' => 'auth'], function ($router) {

    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
    Route::resource('users', 'UserController');
    Route::resource('costumers', 'CostumerController');
    Route::resource('support_types', 'Support_typeController');
    Route::resource('cases', 'CaseController');
    Route::delete('refuse/{id}', 'CaseController@refuseanulation');
    Route::get('numbercases', 'CaseController@numbercases');
    Route::resource('solutions', 'SolutionController');
    Route::post('summarycostumer', 'ReportController@summarycostumer');
    Route::post('summarysession', 'ReportController@summarysession');
    Route::post('printusers', 'ReportController@allusers');
    Route::post('printcustomer', 'ReportController@allcostumers');
    Route::get('printsupportype', 'ReportController@allsupport_types');
        

});

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

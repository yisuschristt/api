<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Recuperación de contraseña Sistema de Gestión de Casos AVAQS CONSULTORES</title>
</head>
<body>
    <center>
        <p style="font-size:15px; color:black"><b>AVAQS CONSULTORES S.A.</b></p>
        <p style="font-size:15px; color:black"><b>RIF: J-29791903-1</b></p>
        <p style="font-size:15px; color:black"><b>TELÉFONOS DE CONTACTO: +584127873237</b></p>
        <br>
        <p style="font-size:15px; color:black"><i>SISTEMA PARA LA GESTIÓN DE CASOS DE AVAQS CONSULTORES S.A.</i></p>
        <hr>
        <p style="font-size:15px; color:black">ESTIMADO USUARIO <h3 style="font-size:25px; color:black"><?php echo $resetPassword->name." ".$resetPassword->lastname."."; ?></h3></p>
        <p style="font-size:15px; color:black">ÉSTE ES EL CÓDIGO DE RECUPERACIÓN PARA EL CAMBIO DE SU CONTRASEÑA:</p>
        <h2 style="font-size:25px; color:blue; display:inline"><strong><?php echo $resetPassword->resetcode; ?></strong></h2>
        <hr>
        <p style="font-size:15px; color:black">UN SALUDO DE PARTE DEL EQUIPO <strong>AVAQS CONSULTORES S.A.</strong></p>
    </center>
</body>
</html>